function Client (profile, dir, cookie, params) {
	const needle = require('needle')
	const path = require('path')
	const self = this
	let debug = require('debug')(`client:${profile.slice(-4)}`)
	debug.enabled = true

	if (cookie) {
		debug('Reusing provided cookie')
		this.cookie = cookie
	}

	async function getNewCookie () {
		// Request full profile page and keep the Cookie headers
		let url = `https://live.kuaishou.com/profile/${profile}`
		debug('Requesting cookie')
		let response = await needle('get', url)
		self.cookie = Object.entries(response.cookies).map(kv => kv.join('=')).join('; ')

		// Activate cookie by calling this magic url with some weird data
		url = `https://live.kuaishou.com/rest/wd/live/web/log`
		const options = {
			headers: {
				Cookie: self.cookie,
				'Content-Type': 'text/plain;charset=UTF-8',
				'Accept': '/*'
			}
		}
		const data = {
			base: {
				session_id: '',
				page_id: '',
			},
			events: [{
				type:'pv',
				data:{
					event_time: Date.now(),
					from: `/`,
					to: `/profile/${profile}`
				}
			}]
		}
		try {
			response = await needle('post', url, JSON.stringify(data), options)
			debug('Cookie activated!')
		} catch (err) {
			debug('Cookie activation failed: %s', err)
			self.cookie = null
		}
	}

	this.findLinks = async function (stopAt) {
		/*
		 * Cookies don't seem to be needed, just visit the page and IP gets validated?
			if (!self.cookie) {
			await getNewCookie()
			if (!self.cookie) {
				return null
			}
		}*/
		const url = `https://live.kuaishou.com/graphql`
		const options = {
			headers: {
				'content-type': 'application/json'
				//,'Cookie': self.cookie
			}
		}
		let pcursor = '' + Date.now(), count = 1000
		if (stopAt) {
			count = 256
		}
		const requestBody = {
			operationName: 'publicFeedsQuery',
			variables: {
				principalId: profile,
				pcursor,
				count
			},
			query: 'query publicFeedsQuery($principalId: String, $pcursor: String, $count: Int) { \
			publicFeeds(principalId: $principalId, pcursor: $pcursor, count: $count) { \
				pcursor live {title playUrls {url} __typename } list { timestamp playUrl expTag __typename } __typename \
		} \
		}'
		}
		let links = [], lastPage = false, badCookies = 0
		while (!lastPage) {
			debug('Requesting up to %s videos created before %s', count, new Date(parseFloat(requestBody.variables.pcursor)))
			const response = await needle('post', url, JSON.stringify(requestBody), options)
			//debug('Response was: %O', response.body)
			if (response.body.errors) {
				debug('API request rejected: %O', response.body.errors)
				return null
			}/*
			if (response.body.data.publicFeeds.pcursor === null) {
				if (badCookies++ > 8) {
					debug('Cookie still not working 😭')
					return null
				}
				debug('Cookie not working, please visit https://live.kuaishou.com/profile/%s and scroll around', profile)
				await delay(10*1000)
				await getNewCookie()
				continue
			}*/
			const responseList = response.body.data.publicFeeds.list
			badCookies = 0
			if (requestBody.variables.pcursor === 'no_more' || responseList.length < count) {
				lastPage = true
			}
			for (const link of responseList) {
				if (link.playUrl && (links.length === 0 || link.playUrl !== links[links.length-1].url)) {
					// Add result
					const name = `kuai-${profile}-${link.timestamp}-${link.playUrl.slice(link.playUrl.lastIndexOf('/')+1)}`
					if (name === stopAt) {
						debug('Found %s, the video we downloaded last time', name)
						lastPage = true
						break
					}
					requestBody.variables.pcursor = link.timestamp
					links.push({ts: link.timestamp, url: link.playUrl, name})
				}
			}
			requestBody.variables.pcursor += ''
		}
		return links
	}

	function delay (n) {
		return new Promise(resolve => setTimeout(resolve, n))
	}

	this.download = async function (link) {
		let downloadPath = params['no-nesting'] ? dir : path.join(dir, profile)
		let retries = 6, delaySeconds = 3
		while (retries--) {
			try {
				await needle('get', link.url, {output: path.join(downloadPath, link.name), read_timeout: 180 * 1000, response_timeout: 15 * 1000})
				return true
			} catch (err) {
				debug('Failed to download %s, will retry', link.name)
				await delay(delaySeconds * 1000)
			}
		}
		if (retries < 0) {
			debug('Download failed, skipping %s', link.url)
			return false
		}
	}

}

module.exports = Client
