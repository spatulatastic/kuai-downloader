const Worker = require('./Worker')
function ConcurrencyLimiter(numWorkers) {
	let unpause
	const jobs = [], sleepingWorkers = []
	const opts = {jobs, sleepingWorkers, paused: null}

	let workerCount = 0

	this.do = function (job, hurry) {
		// If not paused or killing something and a worker is waiting
		let resolver
		let promise = new Promise(resolve => resolver = resolve)
		if (job !== null) {
			job.onComplete = result => resolver(result)
		} else {
			resolver(null)
		}
		if ((!opts.paused || job === null) && sleepingWorkers.length) {
			// Wake a sleeping worker, FILO is fine, pass job via resolve
			sleepingWorkers.pop()(job)
		} else {
			hurry ? jobs.unshift(job) : jobs.push(job)
		}
		return promise
	}

	this.pause = function () {
		// If not already paused
		if (unpause) {
			return
		}
		// Give workers a promise to wait on
		// Keep an unpause handle to resolve that promise
		opts.paused = new Promise(resolve => unpause = resolve)
	}

	this.isPaused = function () {
		return !!unpause
	}

	this.resume = function () {
		if (!unpause) {
			return false
		}
		opts.paused = null
		// Resolve the promise that workers are waiting on
		unpause()
		unpause = null
		return true
	}

	this.setNumWorkers = function (num) {
		// Find the number of workers to start or end
		let workersToStart = num - workerCount
		if (workersToStart > 0) {
			// Spawn new workers
			while(workersToStart--) {
				new Worker(opts)
			}
		} else if (workersToStart < 0) {
			// Null jobs end workers, use hurry flag to jump to the front of queue
			while(workersToStart++) {
				this.doNext(null)
			}
		}
		workerCount = num
	}

	this.getNumWorkers = function () {
		return workerCount
	}

	this.getDepth = function () {
		return jobs.length
	}

	this.sync = function () {
		// Resolves when all workers have caught up to this job
		let workers = workerCount
		let promises = [], resolvers = []
		while (workers--) {
			// Create a promise that resolves when a worker get up to our job
			let resolver
			promises.push(new Promise(resolve => resolver = resolve))
			// Make the worker wait until we let its job complete
			this.do(() => {
				return new Promise(resolve => {
					resolvers.push(resolve)
					resolver()
				})
			})
		}
		// Wait for all our jobs to complete
		return Promise.all(promises).then(() => {
			// Resolve all the workers' promises, letting them continue
			for (resolver of resolvers) {
				resolver()
			}
		})
	}

	this.close = function (immediate) {
		// Start shutting down workers
		// Returns a promise that resolves when nothing else will run
		let resolver
		let promise = new Promise(resolve => resolver = resolve)
		// Get down to last worker
		if (immediate) {
			// Immediate jobs go to the start of the array
			// x x ! x j j j
			this.do(null)
			this.do(resolver, true)
		}
		while (workerCount-- > 1) {
			this.do(null, immediate)
		}
		if (!immediate) {
			// Drain and close, these nulls go to the end of the queue
			// j j j x x ! x
			this.do(resolver)
			this.do(null)
		}
		return promise
	}

	this.setNumWorkers(numWorkers||0)
	return this
}

module.exports = ConcurrencyLimiter
