/*
 * Simple param parser
 *
 * Call with process.argv and arrays/falsey values of wanted params (no dashes)
 * --name=cat or --name cat is acceptable
 * Flags have no value, their presence makes them active
 * Priority is number > string > flag (true)
 * Params array parsed left to right, later params can overwrite earlier ones
 * Ex:
 *   node script.js --count 4 --directory='../../' --infinite-retries
 *   parseParams(process.argv, ['count'], ['directory'], ['infinite-retries']) ✓
 */

function parseParams (argv, numbers, strings, flags, positionalKey = 'positional', allowDoubleDash = false) {

	const positional = [], params = {}
	params[positionalKey] = positional // If 'positional' is a param key, change positionalKey to something else

	for (let i = 2, n = argv.length, arg, val; i < n; i++) {
		arg = argv[i]
		if (arg === '-h' || arg === '--help') {
			return null
		}
		if (arg.length <= 2 || arg.charAt(0) !== '-' || arg.charAt(1) !== '-') {
			positional.push(arg)
			continue
		}
		arg = arg.slice(2)
		let eqIndex = arg.indexOf('=')
		if (eqIndex > 0) {
			val = arg.slice(eqIndex+1)
			arg = arg.slice(0, eqIndex)
		} else {
			val = null
		}
		if (numbers && numbers.includes(arg)) {
			params[arg] = parseFloat(val === null ? argv[++i] : val)
		} else if (strings && strings.includes(arg)) {
			params[arg] = val === null ? argv[++i] : val
		} else if (flags && flags.includes(arg)) {
			params[arg] = true
		} else if (allowDoubleDash) {
				positional.push(arg)
		} else {
			return `Unrecognised option: ${argv[i]}`
		}
	}
	return params
}

module.exports = parseParams
