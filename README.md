# kuaishou-downloader

Downloads all videos from a kuaishou profile page

## Usage

- Install [Node.js](https://nodejs.org/en/)
- Clone/download this repository
- Install dependencies with `yarn install` or `npm install` from within the project directory
- run `node index.js profileId profileId2 profileId3...`, where profileId is:

`https://live.kuaishou.com/profile/AAA18832531100`  
`profileId ------------------------^^^^^^^^^^^^^^`

- Try `--help`/`-h` for more usage information

## Features
- Application can be safely terminated, the current video won't be recorded as complete and will be redownloaded on next run
- Downloaded channels can be updated (download newly released videos by running the script again)
- Downloaded videos can be deleted and won't be redownloaded (as long as the record-profileId.txt file still contains the filename)
- Interrupted/hung downloads are retried several times before giving up
- Clips are downloaded in parallel (default 2, controllable with --parallel=N)
- Download directory can be set with --downloads-dir='../dog-videos'
- All link discovery is performed at start up while the cookie is fresh
- Filename contains the source profile, release timestamp, base64'd video name (not my idea) and probably language. Sorting by name sorts by release date
- Can parse profileId from profile page and video link URLs
- Can just gather direct video links and write them to file (use `--no-downloads --link-file 'link-file.txt'`)

## Cookies
- It looks like cookies are activated by the browser and that whitelists an IP address for a while
- This script will prompt you to visit a profile to activate a cookie if it can't activate one itself
- There's no need to provide a cookie directly to the script, just get the IP whitelisted by visiting a page and scrolling up/down
- Cookies can still be provided by --cookie='../cookie.txt', with the format `client_key=123b09;client_key=3;did=web_aabbcc09;kuaishou.live.bfb1s=aabbcc09` if you want.

## File storage schema
Videos are downloaded as:

- project directory
    - profileId1 directory
        - record-profileId1.txt (keeps track of successfully downloaded files)
        - kwai-profileId1-timestamp1-videoname1.mp4
        - kwai-profileId1-timestamp2-videoname2.mp4
    - profileId2 directory
        - record-profileId2.txt
        - kwai-profileId2-timestamp1-videoname1.mp4
        - kwai-profileId2-timestamp2-videoname2.mp4

The profileIdN directories can be avoided with the --no-nesting flag. The filenames contain enough information to maintain sort order. 

