function Record (profile, dir, params) {
	const path = require('path')
	const fs = require('fs').promises

	let debug = require('debug')(`record:${profile ? profile.slice(-4) : 'link'}`)
	debug.enabled = true
	let fh, contents, workingDir
	if (params['no-nesting'] || !profile) {
		workingDir = dir
	} else {
		workingDir = path.join(dir, profile)
	}

	async function makeWorkingDir() {
		try {
			const dir = await fs.lstat(workingDir)
			if (!dir.isDirectory) {
				throw new Error('Non-directory %s already exists. Destroy it.', profile)
			}
		} catch (ex) {
			await fs.mkdir(workingDir)
		}
	}

	this.load = async function () {
		await makeWorkingDir()
		const filepath = path.join(workingDir, `record-${profile}.txt`)
		debug('Loading', filepath)
		fh = await fs.open(filepath, 'a+')
		contents = (await fh.readFile('utf-8')).split('\n')
		const lastIndex = contents.length - 1
		if (contents.length && contents[lastIndex].length < 2){
			contents.pop()
		}
		if (contents.length) {
			debug('Loaded %d completed downloads', contents.length)
		}
		return contents
	}

	this.last = function () {
		if (!fh || !contents || !contents.length) {
			return null
		}
		return contents[contents.length - 1]
	}

	this.append = function (line) {
		if (!fh) {
			throw new Error('No record file loaded')
		}
		return fh.appendFile(line + '\n').then(() => fh.datasync())

	}
	this.appendLinks = async function (links) {
		if (profile) {
			throw new Error('Tried to append links to wrong kind of record')
		}
		if (!fh) {
			try {
				fh = await fs.open(dir, 'w+')
			} catch (err) {
				debug('Failed to open link file %s: %s', dir, err)
				throw err
			}
		}
		for (link of links) {
			fh.appendFile(link.url + '\n')
		}
	}

	this.close = async function () {
		if (fh) return fh.close()
	}
}

module.exports = Record
