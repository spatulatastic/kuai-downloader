function Worker (opts) {
	/* Opts:
	 * jobs: []
	 * sleepingWorkers: []
	 * paused: null/Promise
	 */
	this.run(opts)
	return this
}

Worker.prototype.run = async function (opts) {
	const jobs = opts.jobs, sleepingWorkers = opts.sleepingWorkers
	let job
	while (!opts.paused || await opts.paused || true) {
		if (!jobs.length) {
			job = await new Promise(resolve => sleepingWorkers.push(resolve))
		} else {
			job = jobs.shift()
		}
		if (job === null) {
			break
		} else if (job instanceof Function) {
			try {
				await job()
				if (job.onComplete) job.onComplete()
			} catch (err) {
				console.log('worker job threw', err)
			}
		} else {
			console.log(`Not executing non-function: ${job}`)
		}
	}
}

module.exports = Worker
