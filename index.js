let fs

const ConcurrencyLimiter = require('./ConcurrencyLimiter')
const parseParams = require('./Params')
const Client = require('./Client')
const Record = require('./Record')

const log = require('debug')('information')
log.enabled = true

main()

async function main () {
	let params = parseParams(process.argv, ['parallel'], ['downloads-dir', 'cookie', 'link-file'], ['no-downloads', 'no-nesting'])
	if (!params || typeof params === 'string') {
		// request for usage information
		printHelp()
		if (params) {
			log(params)
		}
		return
	}
	if (!params.positional.length) {
		printHelp()
		log('No profiles provided')
		return
	}

	fs = require('fs').promises
	let cookie = await loadCookieFromFile(params.cookie), profile
	let linkRecorder
	if (params['link-file']) {
		linkRecorder = new Record(null, params['link-file'], params)
	}
	const limiter = new ConcurrencyLimiter(params.parallel || 2)
	try {
		// Turns URLs into proper profile IDs and deduplicate
		const profiles = Array.from(new Set(params.positional.map(parseProfile)))
		for (profile of profiles) {
			cookie = await startDownload(profile, cookie, limiter, linkRecorder, params)
		}
		log('Link discovery complete')
		await limiter.close()
		if (linkRecorder) {
			linkRecorder.close()
		}
	} catch (err) {
		log('Fatal error: %s', err)
	}
}

function printHelp() {
	console.log(` Usage: node ${process.argv[1].slice(process.argv[1].lastIndexOf('/') + 1)} [profileIds...]`)
	console.log(` Supported options:`)
	console.log(`   --parallel=2               Number of downloads to run concurrently`)
	console.log(`   --downloads-dir='.'        Where to store downloads`)
	console.log(`   --cookie='./cookie.txt'    Load cookie string from this file instead of fetching automatically`)
	console.log(`   --link-file='./links.txt'  Store downloaded links to this file`)
	console.log(`   --no-downloads             Disable downloading, link discovery only (use with link-file)`)
	console.log(`   --no-nesting               Disables creation of subdirectory for each profile`)
}

async function loadCookieFromFile (filename = `./cookie.txt`) {
	try {
		let fh = await fs.open(filename, 'r')
		let cookie = (await fh.readFile('utf-8')).split('\n')[0]
		fh.close()
		log('Loaded cookie %s from cookie.txt', cookie)
		return cookie
	} catch (err) {
		return null
	}
}

function parseProfile(str) {
	if (str.indexOf('kuaishou.com') === -1) {
		return str
	}
	return str.match(/.*kuaishou\.com\/.*?\/(.*?)(\/|$)/)[1]
}

function parseDownloadDir(p) {
	return require('path').resolve(p || '.')
}

async function startDownload (profile, cookie, limiter, linkRecorder, params) {
	const downloadDir = parseDownloadDir(params['downloads-dir'])
	const record = new Record(profile, downloadDir, params)
	const downloaded = await record.load()
	const client = new Client(profile, downloadDir, cookie, params)
	const allLinks = await client.findLinks(record.last())
	async function end () {
		await record.close()
		return client.cookie
	}

	const toDo = []
	if (!allLinks) {
		log('Failed to load links, cookie activation may have changed')
		end()
		throw new Error('Unable to authenticate')
	}
	if (allLinks.length === 0) {
		log(`Didn't find anything to download`)
		return end()
	}
	for (const link of allLinks) {
		if (!downloaded.includes(link.name)) {
			toDo.push(link)
		}
	}
	toDo.reverse()
	if (linkRecorder) {
		log('Appending %d links to link file', toDo.length)
		linkRecorder.appendLinks(toDo)
	}
	if (params['no-downloads']) {
		return end()
	}
	if (toDo.length === 0) {
		log('All clips have already been downloaded')
		return end()
	}
	log('There are %d links to download (%s already downloaded)', toDo.length, downloaded.length)
	const jobs = []
	for (let i = 0, n = toDo.length, link; i < n; i++) {

		jobs.push(limiter.do(async () => {
			let link = toDo[i]
			log('Downloading (%d/%d/%d): %s', i+1, n, limiter.getDepth(), link.name, )
			if (await client.download(link)) {
				await record.append(link.name)
			}
		}))
	}
	Promise.all(jobs).then(() => {
		log('All downloads for %s complete', profile)
		record.close()
	})
	log('Downloads for %s queued, continuing link discovery', profile)
	return client.cookie
}

